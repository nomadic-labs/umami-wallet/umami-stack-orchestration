[[_TOC_]]

# Context

This run book explains how to perform simple actions on each of the environments configured currently with docker-compose.
Most of the usual commands have been integrated in Makefiles to make sure their usage is consistent.

Longer procedures should be split in independent pages, and linked here if relevant.


# Initialization 


```
cd /opt/
git clone --branch docker-compose --single-branch git@gitlab.com:nomadic-labs/umami-wallet/umami-backend.git
cd /opt/umami-backend/amino/
make install-docker-compose
```

Notes : 
- some comands assume that you are in `/opt/umami-backend/amino/<env>/` path.

# General
Those actions are to be run in `/opt/umami-backend/amino/` since they work regardless of the environment.



## Versions and nginx api mapping

To check what is served where, run `make versions` from the base directory :
```
cd /opt/umami-backend/amino
make versions
```

This will generate a "slack friendly" message showing where each of the environments are published.


# Tezos_net Environments
All _tezos_net environments_ are using a set of configuration files symlinked to the corresponding directory, with a custom `.env` file:

`cd /opt/umami-backend/amino/<tezos_net>/`
with `tezos_net` in { `mainnet`,  `edo2net`, `florencenet`, `granadanet`, ... }

- config files 
    - `Makefile` (symlink to `../config/Makefile`)
    - `docker-compose.yml` (symlink to `../config/docker-compose.yml`)
- different `.env` file per _tezos_net environments_ to provide flexibility over the ports and variables used (heavily used in the `Makefile` and `docker-compose.yml`), with a symlink :
    - (default)) `umami-backend.env.default` (if no hostname specific file)
    - `umami-backend.env.<tezos_net>.<hostname -s>` : `umami-backend.env.mainnet.api`, `umami-backend.env.granadanet.dev-api`, ... 



```mermaid
graph TD
 subgraph Production Server

  subgraph Nginx reverse-proxy
    pm1["/mainnet"]
    pf["/florencenet"]
    pg["/granadanet"]
  end

    dcm1["Tez Env : mainnet"]
    dcf["Tez Env : florencenet"]
    dcg["Tez Env : granadanet"]
 end

pm1 --> dcm1
pf --> dcf
pg --> dcg


 subgraph QA Server

  subgraph Nginx reverse-proxy
    qpm1["/mainnet"]
    qpf["/florencenet"]
    qpg["/granadanet"]
  end

    qdcm1["Tez Env : mainnet"]
    qdcf["Tez Env : florencenet"]
    qdcg["Tez Env : granadanet"]
 end

qpm1 --> qdcm1
qpf --> qdcf
qpg --> qdcg
```


## Environment Initialization

We rely on the presence of either :
* `.env` file, symlink to either :
   * `umami-backend.env.default`
   * `umami-backend.env.$(tezos_net).$(hostname -s)` (so, either `umami-backend.env.mainnet.api`, `umami-backend.env.mainnet.qa-api`,  `umami-backend.env.mainnet.dev-api`, `umami-backend.env.florencenet.qa-api`, `umami-backend.env.florencenet.api`, ... )

To create the link (only) :
```
cd /opt/umami-backend/amino/<env>/
make config-link
```

## Start Environment

```
cd /opt/umami-backend/amino/<env>/
make up
```

Notes : 
* this will create the link if not present already 
* this will download the specified docker images
* this will create the docker volumes, docker networks, etc.
* this will start the db, node, apply the DB schema, then start the indexers and mezos.

## Environment Status 

`cd /opt/umami-backend/amino/<env>/`

```
make ps

# it will run :
# sudo docker-compose  ps
# sudo docker ps | grep <env>_
```

## Upgrades

`cd /opt/umami-backend/amino/<env>/`

then edit the .env file, in particular the reference to the docker-image you wish to use 

```
make up
```

Note : 
- this will start the tezos-node and postgres DB, then apply the DB schema, and only then start mezos and the indexers (mempool and token-support) .

### tezos-node upgrade
1. `cd /opt/umami-backend/amino/<env>/`
1. edit .env
1. `make update-node`

### Indexer upgrades
 Depending on the release nodes of the indexer, you might need to perform a full reindexing of the chain.

#### indexer upgrade : _with_ full chain reindexing (Multicore)
1. `cd /opt/umami-backend/amino/<env>/`
1. edit .env
1. `make fast-indexer`

Notes : 
- this is mainly used for `mainnet` and has not been tested for other networks.
- this depends on an already generated list of contracts and block cache present
- the Multicore script does drop the database, that's why this doesn't have to be explicitely done manually

Time considerations :
- on `mainnet`, in june 2021, with the indexer `v9.3.x` this was taking around 5h.
- on `mainnet`, in october 2021, with the indexer `v9.7.x` this was taking around 8h.
 

#### indexer upgrade : _with_ full chain reindexing (regular / not multicore)
1. `cd /opt/umami-backend/amino/<env>/`
1. edit .env
1. Drop the DB by removing temporarily unused volumes and relaunch the indexing :
  - step by step commands with prompt confirmation
```
sudo docker-compose rm -v -s postgres indexer-mempool indexer-token-support
sudo docker volume prune
make up
```
  - one step command without confirmation
```
  sudo docker-compose rm -v -s -f postgres indexer-mempool indexer-token-support && sudo docker volume prune -f && make up
```

#### indexer upgrade : without reindexing
This will just upgrade the docker image and re-apply the db schema, but will not cause a full reindex.
1. `cd /opt/umami-backend/amino/<env>/`
1. edit .env
1. `make up`



## Pause / Partial Termination
1. `cd /opt/umami-backend/amino/<env>/`
1. stop everything but keep the data (persisted on docker named volumes)
```
sudo docker-compose stop
```


## Full Termination
**Note** : you will loose everything !

1. `cd /opt/umami-backend/amino/<env>/`
1. stop everything and remove the data persisted on docker named volumes :
`sudo docker-compose down -v `

**Note** : you will loose everything !


# Other operations

<!-- ## block statuses -->


## node backup, transfer and restore
1. `cd /opt/umami-backend/amino/<env>/`
1. Generate a node backup : `make node_backup`
1. Transfer the node backup : `make node_backup_transfer_to_<host>` with host in `{dev,qa,api}`
1. Restore a node backup : `make node_backup_restore`

Note : 
- This generate a `tgz` and transfers it through `scp` with rsync
- As of 2021-July-05, a `mainnet` tgz archive will be around 113GB.
