#!/bin/bash

# sudo mkdir /var/log/umami-stack/
## sudo chmod 777 /var/log/umami-stack/
### sudo chown tezos:tezos /var/log/umami-stack/
### sudo su tezos 
# crontab -e

#run every minute from 03/dec to 04/dec
# * * 03-04 12 * /opt/umami-stack/amino/config/upg_mainnet_hangzhou.sh >> /var/log/umami-stack/upg_mainnet_hangzhou 2>&1
#run every minute from 03/dec to 04/dec
# * * 03-04 12 * /opt/umami-backend/amino/config/upg_mainnet_hangzhou.sh  >> /var/log/umami-stack/upg_mainnet_hangzhou 2>&


# tail -f /var/log/umami-stack/upg_mainnet_hangzhou

echo "$(date) - $0 -- start"
# for env in mainnet 
# do 
	env="mainnet"
	echo "$(date) - $0 - $env"

	make -f /opt/umami-backend/amino/${env}/Makefile -C /opt/umami-backend/amino/${env}/ upg_mezos_hangzhou 2>&1
	sudo docker inspect ${env}_mezos_1 | grep MEZOS_PROTO

# done
echo "$(date) - $0 -- end"
