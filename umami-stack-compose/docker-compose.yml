version: "3.6"

# persistent docker volumes
volumes:
  client_data: 
  node_data: 
  database_data: 
  node_data_tezos_metrics:

networks:
  database: {}
  node: {}
  frontend:
    external: true
    name: traefik-proxy
      
services:
  # tezos node : octez 
  node:
    image: ${node_docker_image}
    hostname: node
    command: octez-node --net-addr :${node_port_p2p} --network ${octez_tezos_net} --history-mode=${octez_history_mode} 
                  --rpc-addr          :${node_port_rpc} --allow-all-rpc          :${node_port_rpc} 
                  --rpc-addr localhost:${node_port_rpc} --allow-all-rpc localhost:${node_port_rpc} 
                  --rpc-addr      node:${node_port_rpc} --allow-all-rpc      node:${node_port_rpc}
                  --metadata-size-limit unlimited
                  --metrics-addr 0.0.0.0:${node_metrics_port}
                  #--connections 1
                  #--private-mode
    #environment:
    #        TEZOS_CONTEXT : v
    #        TEZOS_LOG :  "* -> debug"
    #        TEZOS_LOG : 'rpc -> info'
    ports:
      - ${node_port_rpc}:${node_port_rpc}
      - ${node_port_p2p}:${node_port_p2p}
    expose:
      - ${node_port_rpc}
      - ${node_port_p2p}
    volumes:
      - node_data:/var/run/tezos/node
      - client_data:/var/run/tezos/client
      - ./import-data:/import-data
    restart: always
    healthcheck:
      test: ["CMD", "wget", "--tries=1", "--spider", "http://localhost:${node_port_rpc}/version"]
      interval: 10s
      timeout: 5s
      start_period: 120s
    networks:
      - node
    mem_limit:     ${node_mem_limit}
    memswap_limit: ${node_memswap_limit}
    mem_swappiness: 60

  # octez node with rolling value to be set appropriately for the indexer to catch-up
  node_sync:
    image: ${node_docker_image}
    hostname: node_sync
    command: octez-node --net-addr :${node_port_p2p} --rpc-addr :${node_port_rpc} --network ${octez_tezos_net} --allow-all-rpc :${node_port_rpc} --history-mode=${octez_history_mode}
    networks:
      - node
    ports:
      - ${node_port_rpc}:${node_port_rpc}
      - ${node_port_p2p}:${node_port_p2p}
    expose:
      - ${node_port_rpc}
      - ${node_port_p2p}
    volumes:
      - node_data:/var/run/tezos/node
      - client_data:/var/run/tezos/client
      - ./import-data:/import-data
    healthcheck:
      test: ["CMD", "wget", "--tries=1", "--spider", "http://localhost:${node_port_rpc}/version"]
      interval: 10s
      timeout: 5s
    mem_limit:     ${node_mem_limit}
    memswap_limit: ${node_memswap_limit}
    mem_swappiness: 60
    deploy:
      restart_policy:
        condition: on-failure

  # tezos node : will run in case of protocol upgrades
  upgrader:
    image: ${node_docker_image}
    command: tezos-upgrade-storage
    volumes:
      - node_data:/var/run/tezos/node
      - client_data:/var/run/tezos/client
    deploy:
      restart_policy:
        condition: on-failure
        # delay: 50s

  # DB : postgre DB for indexed data.
  # Write from the indexers and read from mezos
  # https://hub.docker.com/_/postgres/
  postgres:
    image: ${postgres_docker_image}
    environment:
      POSTGRES_USER: ${POSTGRES_USER:-postgres}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      PGDATA: /data/postgres
      POSTGRES_HOST_AUTH_METHOD: trust 
    env_file:
      ./.env
    volumes:
       - database_data:/data/postgres
       # create DB and user for indexer (iff not existing)
       - ./build/postgres/indexer-db-create.sh:/docker-entrypoint-initdb.d/initdb.sh
       - ./import-data:/import-data
    networks:
      - database
    expose:
      - 5432
    restart: always
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U ${POSTGRES_USER}"]
      interval: 10s
      timeout: 5s

  # indexer : create the schema
  indexer-db-schema:
    build: 
      context: ./build/indexer-db-schema
      dockerfile: Dockerfile
      args:
        image_ref: ${indexer_docker_image}
    command: "${DBURL}"
    env_file:
      ./.env
    networks:
      - database
    restart: on-failure
    depends_on:
      postgres:
        condition: service_healthy

  # indexer : mempool
  indexer-mempool:
    image: ${indexer_docker_image}
    command: --mempool-only 
              --db=${DBURL} --tezos-url=http://node:${node_port_rpc} --verbose --debug --verbosity=1 
              --no-snapshots 
              --no-contract-balances
              --textual-rpc
    depends_on:
      postgres:
        condition: service_healthy
      node:
        condition: service_healthy
    restart: always
    networks:
      - node
      - database

  # indexer : chain & tokens-support
  indexer-chain:
    image: ${indexer_docker_image}
    command: --tokens-support 
             --db=${DBURL} --tezos-url=http://node:${node_port_rpc} --verbose --debug --verbosity=1 
             --no-contract-balances 
             --no-big-maps
             --do-not-get-missing-scripts 
             --heads-only ${INDEXING_FROM} 
    depends_on:
      postgres:
        condition: service_healthy
      node:
        condition: service_healthy
    restart: always
    networks:
      - node
      - database
    mem_limit:      "32Gb"
    memswap_limit:  "32Gb" 
    mem_swappiness: 60

  # mezos : webservice serving RPC for umami
  mezos:
    image: ${mezos_docker_image}
    container_name: ${tezos_net}-mezos
    hostname: ${tezos_net}-mezos
    # entrypoint: ./mezos.exe
    command: --chain-db="${DBURL}" --tezos-url=http://node:${node_port_rpc} --mezos-url="http://0.0.0.0:${mezos_port_rpc}" --more-logs --verbosity=debug
    env_file:
      ./.env
    environment:
      NODE_URL: http://node:${node_port_rpc}
    ports:
      - ${mezos_port_rpc}:${mezos_port_rpc}
        #expose:
        #- ${mezos_port_rpc}
    networks:
      - node
      - database
      - frontend
    depends_on:
      postgres:
        condition: service_healthy
      node:
        condition: service_healthy
      indexer-chain:
        condition: service_started
      indexer-mempool:
        condition: service_started
    restart: always
    healthcheck:
      test: ["CMD", "wget", "--tries=1", "http://${tezos_net}-mezos:${mezos_port_rpc}/version", "--output-file=/dev/null"]
      interval: 10s
      timeout: 5s

  # umami-wallet proxy for using the cryptonomics endpoint for NFTs
  graphic-proxy:
    image: ${graphic_proxy_docker_image}
    command: --port=${graphic_proxy_port} --cryptonomickey=${graphic_proxy_cryptonomics_key}
    ports:
      - ${graphic_proxy_port}:${graphic_proxy_port}
    expose:
      - ${graphic_proxy_port}
    networks:
      - node
      - frontend
    restart: unless-stopped
    healthcheck:
       test: ["CMD", "wget", "--tries=1", "--spider", "http://graphic-proxy:${graphic_proxy_port}/check"]
       interval: 30s
       timeout: 10s

###############################################################################
######## Monitoring 
###############################################################################

  postgres-exporter:
    image: prometheuscommunity/postgres-exporter:latest
    #image: bitnami/postgres-exporter:latest
    networks:
      - database
      - frontend
    ports:
      - ${prom_postgres_exporter}:9187
    restart: always
    environment:
      DATA_SOURCE_NAME: ${DBURL}?sslmode=disable
    healthcheck:
      test: ["CMD", "wget", "--tries=1", "--spider", "http://localhost:9187/metrics"]
      interval: 10s
      timeout: 5s


        #  whoami:
        #    image: "traefik/whoami"
        #    container_name: "whoami"
        #    networks:
        #      - frontend 
        #      - node
        #      - database
        #    ports:
        #      - 90:80
        #    labels:
        #      - traefik.enable=true
        #      - traefik.docker.network=traefik-proxy
        #      - traefik.http.routers.whoami.rule=Host(`whoami-${tezos_net:-default}.${DOMAIN}`)
        #      - traefik.http.routers.whoami.entrypoints=websecure
        #      - traefik.http.services.whoami.loadbalancer.server.port=80
      

