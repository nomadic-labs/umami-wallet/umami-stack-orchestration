#!/bin/bash
set -e

#psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
psql "postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB?sslmode=disable" <<-EOSQL
-- without password
create user ${DB_USER} ;
-- with password
-- create user ${DB_USER} with encrypted password '${DB_PASS}';
create database ${DB_NAME};
grant all privileges on database ${DB_NAME} to ${DB_USER};

-- modify for SSD performances (cf. https://pgtune.leopard.in.ua )
ALTER SYSTEM SET
 random_page_cost = '1.1';
ALTER SYSTEM SET
 effective_io_concurrency = '200';

EOSQL
