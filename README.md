
## Why Umami Stack Orchestration

The tezos Umami-Wallet clients need an API backend to access tezos blockchain data, this project is the orchestration of those components.

| component          | project URL | latest | How we use it |
| ---                | ---                                            | ---| ---|
| octez (tezos-node) | https://gitlab.com/tezos/tezos/                | [![releases-octez](https://badgen.net/gitlab/release/tezos/tezos)](https://gitlab.com/tezos/tezos/-/releases) | connection with the tezos blockchain |
| tezos-indexer      | https://gitlab.com/nomadic-labs/tezos-indexer  | [![releases](https://badgen.net/gitlab/release/nomadic-labs/tezos-indexer)](https://gitlab.com/nomadic-labs/tezos-indexer/-/releases) | indexing data from the node to a database | 
| postgres           |                                                | (latest) | Database storing indexed data |
| mezos              | https://gitlab.com/nomadic-labs/mezos          | [![releases](https://badgen.net/gitlab/release/nomadic-labs/mezos)](https://gitlab.com/nomadic-labs/mezos/-/releases) | API interface for the indexer |


| (known) clients          | project URL | latest | How we use it |
| ---                | ---                                            | ---| ---|
| umami              | https://gitlab.com/nomadic-labs/umami-wallet/umami        | [![releases](https://badgen.net/gitlab/release/nomadic-labs%2Fumami-wallet/umami)](https://gitlab.com/nomadic-labs/umami-wallet/umami/-/releases) | Desktop client tezos wallet |
| umami-mobile       | https://gitlab.com/nomadic-labs/umami-wallet/umami-mobile | [![releases](https://badgen.net/gitlab/release/nomadic-labs%2Fumami-wallet/umami-mobile)](https://gitlab.com/nomadic-labs/umami-wallet/umami-mobile/-/releases) | Mobile (iOS, Android) client tezos wallet |


## Umami-stack-orchestration Architecture

Each  _Umami-stack API environments_ orchestrate a set of the following components:
 * 1 tezos-node `octez` only accessible locally for the tezos-indexer
 * 2 tezos-indexer (token-support and mempool)
 * 1 mezos for API interface
 * 1 database for the tezos-indexers and mezos

Notes :
* Umami front-end clients can only contact mezos directly (the database is not accessible).
* Umami front-end clients are not provided RPC access to the nodes started for the indexers and for mezos, we suggest them to rely on public nodes or start your own.

```mermaid
graph RL
    u[Umami clients]

  subgraph outside interactions
    p2p
    c[cryptonomics moderation]
    pn[tezos-node public]
  end

  subgraph "Umami-stack API Environement"
    mez[mezos]
    db[db]
    idx1[tezos-indexer & tokens-support]
    idx2[tezos-indexer mempool]
    node[tezos-node]
    g[graphic-proxy]
  end

  u --> mez
  u --> g
  
  mez & idx1 & idx2 -.-> db 
  idx1 & idx2 --> node
  node --> p2p
  mez --> node

  g --> c

  pn --> p2p
  u --> pn
```





## Technical organisation

### docker-compose
* `umami-stack-compose/docker-compose.yml` defines the orchestration of the different services running together.
* All environment variables are configured in the `umami-stack-compose/.env` file, by default for mainnet.
* The `umami-stack-compose/Makefile` defines helpful commands for recurring tasks and maintenance operations (start, stop, update, ...)

![Orchestration Docker-compose de Umami Backend pour 1 net (sans monitoring)](./images/docker-compose.png "Orchestration Docker-compose de Umami Backend pour 1 net (sans monitoring)")


### Running Locally with Docker-Compose

1. clone this current repository
2. edit `umami-stack-orchestration/umami-stack-compose/.env` to fit your values 
4. `cd umami-stack-orchestration/umami-stack-compose && make up`

Note: by default this will start an `octez` tezos node running on the `mainnet` blockhain network in archive mode, which will take about 2 weeks to fully download from the peer to peer layers.

 See `umami-stack-compose/Makefile` specific targets to import archives from other sources that you trust.
 
 `make import-archive-tarball` for example, needs to be run after `make up` and only once the node is fully initialized a first time since the node identity needs to be generated locally before importing the latest archive tarball from xtz-shots.io (takes about 2h depending on your network).


## License
This project is shared under the MIT Licence.

## Authors and acknowledgment
Nomadic-Labs (2021-2023)
* @comeh              Corentin Méhat (2021-2023) / Nomadic-Labs
* @philippewang.info  Philippe Wang  (2021-2023) / Nomadic-Labs
* @SamREye            Samuel Bourque (2021-2022) / Nomadic-Labs

## Project status
This project is actively used for orchestration of all backends environments used by `umami-mobile` and `umami` (desktop) clients : Dev, QA, and Production, as a git submodule in the corresponding project.
